import Controllers.UserController;
import DAOImplementation.UserPOIService;
import Utils.ViewUtils;

import static spark.Spark.*;


/**
 * Created by jafet on 1/10/2016.
 */
public class Home {


  public static void main(String[] a){

    port(8100);
    staticFiles.location("/public/");



    new UserController( new UserPOIService());

    get("/", (req, res) -> ViewUtils.renderIndex(req));

    after((req, res) -> {
      res.type("text/html");
    });

  }
}

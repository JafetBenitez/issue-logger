package Utils;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.*;

/**
 * Created by jafet on 7/10/2016.
 */
public class ApachePOIUtils {

  public ApachePOIUtils(){

  }
  public static HSSFWorkbook initializeWorkBook() {

    File file = new File("IssueLogger.xls");

    if (!file.exists()){
      HSSFWorkbook wb = null;
      try {
          file.createNewFile();

          wb = new HSSFWorkbook();

          HSSFSheet userSpreadSheet = wb.createSheet("Usuarios");
          HSSFSheet isuuesSpreadSheet = wb.createSheet("Incidentes");
          HSSFSheet teamsSpreadSheet = wb.createSheet("Equipos");

          String[] userFields = new String[]{"nick", "password", "name", "team"};

          HSSFRow header = userSpreadSheet.createRow(0);

          int counter = 0;

          for(String itm : userFields)
          {
            header.createCell(counter).setCellValue(userFields[counter]);
            counter++;
          }


          FileOutputStream fileOut = new FileOutputStream(file);
          wb.write(fileOut);
          fileOut.close();

        }
        catch (IOException e){
          e.printStackTrace();
        }
      return wb;
    }

    else{

      HSSFWorkbook wb = null;
      try {
        wb = new HSSFWorkbook(new FileInputStream("IssueLogger.xls"));
      }
      catch (IOException e) {e.printStackTrace();}
      return wb;
    }



  }
}

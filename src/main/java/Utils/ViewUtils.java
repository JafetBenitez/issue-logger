package Utils;
import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.Request;
import spark.template.velocity.VelocityTemplateEngine;



/**
 * Created by jafet on 2/10/2016.
 */
public class ViewUtils {

  public static String renderIndex(Request req) {
    String statusStr = req.queryParams("status");
    Map<String, Object> model = new HashMap<>();
 /*   model.put("todos", TodoDao.ofStatus(statusStr));
    model.put("filter", Optional.ofNullable(statusStr).orElse(""));
    model.put("activeCount", TodoDao.ofStatus(Status.ACTIVE).size());
    model.put("anyCompleteTodos", TodoDao.ofStatus(Status.COMPLETE).size() > 0);
    model.put("allComplete", TodoDao.all().size() == TodoDao.ofStatus(Status.COMPLETE).size());
    model.put("status", Optional.ofNullable(statusStr).orElse(""));
    if ("true".equals(req.queryParams("ic-request"))) {
      return renderTemplate("velocity/todoList.vm", model);
    }*/
    //return renderTemplate("velocity/index.vm", model);

   return strictVelocityEngine().render(new ModelAndView(model, "velocity/index.vm"));


  }

  private static VelocityTemplateEngine strictVelocityEngine() {
    VelocityEngine configuredEngine = new VelocityEngine();
    configuredEngine.setProperty("runtime.references.strict", true);
    configuredEngine.setProperty("resource.loader", "class");
    configuredEngine.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    return new VelocityTemplateEngine(configuredEngine);
  }

}



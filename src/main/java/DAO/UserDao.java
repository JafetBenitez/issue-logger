package DAO;

import Models.User;
import spark.Request;

import java.util.List;
import java.util.Set;

/**
 * Created by jafet on 2/10/2016.
 */
public interface UserDao {


  public int createUser(String name, String nick, String pass, String team);
  public int createUser(Request request);
  public List<User> getAllUsers();
  public User getUser(String rollNo);
  public void updateUser(User us);
  public void deleteUSer(User us);
}

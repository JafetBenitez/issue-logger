package Models;

/**
 * Created by jafet on 2/10/2016.
 */
public class User {

  private int id;
  private String name;
  private String nick;
  private String password;
  private String team;


  public User() {
  }

  public  User(String name, String login, String pass, String team){
    this.nick = login;
    this.name = name;
    this.password = pass;
    this.team = team;
  }


  public int getId() {
    return id;
  }

  public String getTeam() {
    return team;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNick() {
    return nick;
  }

  public void setNick(String login) {
    this.nick = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setTeam(String team) {
    this.team = team;
  }










}

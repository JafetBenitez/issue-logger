package DAOImplementation;

import Controllers.UserController;
import DAO.UserDao;
import Models.User;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Spark;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static Utils.ApachePOIUtils.initializeWorkBook;


/**
 * Created by jafet on 7/10/2016.
 */
public class UserPOIService implements UserDao {

  List<User> users = new ArrayList<>();
  User u = null;
  private final Logger logger = LoggerFactory.getLogger(UserController.class);



  public UserPOIService(){


  }

  @Override
  public  int createUser(String name, String nick, String pass, String team) {


    HSSFWorkbook wb =  initializeWorkBook();
    HSSFSheet sheet = wb.getSheet("Usuarios");

    HSSFRow row = sheet.createRow(sheet.getLastRowNum()+1);

    row.createCell(0).setCellValue(nick);
    row.createCell(1).setCellValue(pass);
    row.createCell(2).setCellValue(name);
    row.createCell(3).setCellValue(team);

    File file = new File("IssueLogger.xls");

    FileOutputStream fileOut = null;
    try {
      fileOut = new FileOutputStream(file);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    try {
      wb.write(fileOut);
    } catch (IOException e) {
      e.printStackTrace();
    }
    try {
      fileOut.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
   return 200;
  }

  @Override
  public  int createUser(Request request) {

    User u = new User(request.queryParams("nick"),
      request.queryParams("name"),
      request.queryParams("password"),
      request.queryParams("team"));
    HSSFWorkbook wb =  initializeWorkBook();
    HSSFSheet sheet = wb.getSheet("Usuarios");

    HSSFRow row = sheet.createRow(sheet.getLastRowNum()+1);

    row.createCell(0).setCellValue(u.getNick());
    row.createCell(1).setCellValue(u.getPassword());
    row.createCell(2).setCellValue(u.getName());
    row.createCell(3).setCellValue(u.getTeam());

    File file = new File("IssueLogger.xls");

    FileOutputStream fileOut = null;
    try {
      fileOut = new FileOutputStream(file);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    try {
      wb.write(fileOut);
    } catch (IOException e) {
      e.printStackTrace();
    }
    try {
      fileOut.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return 200;
  }


  @Override
  public List<User> getAllUsers() {
    logger.info("Cargando usuarios...");
    HSSFWorkbook wb =  initializeWorkBook();

    HSSFSheet sheet = wb.getSheet("Usuarios");

    logger.info(sheet.toString());
    for (Row r : sheet){
      User u = new User();
      u.setName(r.getCell(0).getStringCellValue());
      u.setNick(r.getCell(1).getStringCellValue());
      u.setTeam(r.getCell(3).getStringCellValue());
      logger.info(u.getName());

      users.add(u);
      logger.info(users.toArray().toString());
    }


    return this.users;
  }


  @Override
  public User getUser(String id) {
    logger.info("Cargando usuario...");
    logger.info(id);

    if (id != null){
      int record = Integer.parseInt(id);
      logger.info("Cargando usuario...");

      HSSFWorkbook wb =  initializeWorkBook();
      HSSFSheet sheet = wb.getSheet("Usuarios");

      Row  r = sheet.getRow(record+1);

      logger.info(r.toString());
      User u = new User();
      u.setName(r.getCell(0).getStringCellValue());
      u.setNick(r.getCell(1).getStringCellValue());
      u.setTeam(r.getCell(3).getStringCellValue());

      return u;
    }
    else{
      User u = new User();
      return u;
    }


  }

  @Override
  public void updateUser(User us) {
    this.users.get(us.getId()).setName(us.getName());
    System.out.println("Student: Roll No " + us.getId() + ", updated in the database");
  }

  @Override
  public void deleteUSer(User us) {
    this.users.remove(us.getId());
    System.out.println("Student: Roll No " + us.getId() + ", deleted from database");
  }
}

//package DAOImplementation;
//
//import DAO.UserDao;
//import Models.User;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by jafet on 2/10/2016.
// */
//public class UserArrayService implements UserDao{
//
//  List<User> users;
//
//  public UserArrayService(){
//    this.users = new ArrayList<User>();
//    User student1 = new User("Robert","user1", "pass");
//    User student2 = new User("John","user2", "pass");
//    this.users.add(student1);
//    this.users.add(student2);
//  }
//
//  //retrive list of students from the database
//  @Override
//  public List<User> getAllUsers() {
//    return this.users;
//  }
//
//  @Override
//  public User getUser(int id) {
//    return users.get(id);
//  }
//
//  @Override
//  public void updateUser(User us) {
//    this.users.get(us.getId()).setName(us.getName());
//    System.out.println("Student: Roll No " + us.getId() + ", updated in the database");
//  }
//
//  @Override
//  public void deleteUSer(User us) {
//    this.users.remove(us.getId());
//    System.out.println("Student: Roll No " + us.getId() + ", deleted from database");
//  }
//}
//
//

package Controllers;
import DAO.UserDao;
import DAOImplementation.UserPOIService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static Utils.JsonUtil.json;
import static spark.Spark.*;

/**
 * Created by jafet on 2/10/2016.
 */


public class UserController {

  private final Logger logger = LoggerFactory.getLogger(UserController.class);

  public  UserController(final UserDao udao){
    logger.info("Instanciando controlador UserController");

    UserDao userService = new UserPOIService();

    get("/api/users", (req, res)-> userService.getAllUsers(), json());

    post("/api/users", (request, response) -> userService.createUser(request), json());

    get("api/users/:id", (request, response) -> userService.getUser(request.params(":id")), json());

    after((req, res) -> res.type("application/json"));
  }
}
